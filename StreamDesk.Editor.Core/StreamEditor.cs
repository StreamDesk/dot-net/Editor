﻿using NasuTek.DevEnvironment;
using NasuTek.DevEnvironment.Extensibility.Workbench;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using NasuTek.DevEnvironment.Extensibility;
using NasuTek.M3.Streams.Database;

namespace StreamDesk.Editor.Core
{
    public partial class StreamEditor : DevEnvDocument
    {
        public StreamEditor()
        {
            InitializeComponent();
        }

        public override void Open(DocumentMetadata openObj) {
            propertyGrid1.SelectedObject = openObj.DataObject;

            RefreshDocument();
        }

        public override void RefreshDocument() {
            if (propertyGrid1.SelectedObject is Stream) {
                Text = ((Stream) propertyGrid1.SelectedObject).Name;
            } else if (propertyGrid1.SelectedObject is Provider) {
                Text = ((Provider) propertyGrid1.SelectedObject).Name;
            } else if (propertyGrid1.SelectedObject is StreamEmbed) {
                Text = ((StreamEmbed) propertyGrid1.SelectedObject).Name;
            } else if (propertyGrid1.SelectedObject is StreamsDatabase) {
                Text = ((StreamsDatabase) propertyGrid1.SelectedObject).Name;
            }
        }

        public override bool IsSameDocument(DocumentMetadata newDocument)
        {
            if (propertyGrid1.SelectedObject is Stream && newDocument.DataObject is Stream)
            {
                var obj = ((Stream) propertyGrid1.SelectedObject);
                var newObj = ((Stream) newDocument.DataObject);

                return obj.Name + obj.StreamGuid == newObj.Name + newObj.StreamGuid;
            }
            else if (propertyGrid1.SelectedObject is Provider && newDocument.DataObject is Provider)
            {
                var obj = ((Provider)propertyGrid1.SelectedObject);
                var newObj = ((Provider)newDocument.DataObject);

                return obj.Name + obj.Description == newObj.Name + newObj.Description;
            }
            else if (propertyGrid1.SelectedObject is StreamEmbed && newDocument.DataObject is StreamEmbed)
            {
                var obj = ((StreamEmbed)propertyGrid1.SelectedObject);
                var newObj = ((StreamEmbed)newDocument.DataObject);

                return obj.Name + obj.EmbedFormat == newObj.Name + newObj.EmbedFormat;
            }
            else if (propertyGrid1.SelectedObject is StreamsDatabase && newDocument.DataObject is StreamsDatabase)
            {
                var obj = ((StreamsDatabase)propertyGrid1.SelectedObject);
                var newObj = ((StreamsDatabase)newDocument.DataObject);

                return obj.Name == newObj.Name;
            }
            return false;
        }

        public override bool IsSameDocument(object obj) {
            var dm = new DocumentMetadata();
            dm.Metadata.Add(obj);
            return IsSameDocument(dm);
        }
    }
}
