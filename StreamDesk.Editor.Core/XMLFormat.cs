﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using NasuTek.DevEnvironment;
using NasuTek.DevEnvironment.Extensibility;
using NasuTek.DevEnvironment.Extensibility.Project;
using NasuTek.DevEnvironment.Extensibility.Workbench;
using NasuTek.M3.Streams;
using NasuTek.M3.Streams.Database;
using NasuTek.M3.Streams.UiInterfaces;
using Stream = NasuTek.M3.Streams.Database.Stream;

namespace StreamDesk.Editor.Core {
    public class CantLoadProject : IProject {
        private class CantLoadObject : IObject {
            public Guid DocumentType {
                get { return Guid.Empty; }
            }

            public string Name { get; set; }

            public new object Object { get; set; }

            public object[] PropertyObjects {
                get { return ProjectService.EmptyObjects; }
            }
        }

        private class CantLoadFolder : IFolder {
            private CantLoadObject m_CantLoadObject = new CantLoadObject();

            public void AddFolder(IFolder folder) {}

            public void AddObject(IObject objectd) {}

            public string Name { get; set; }

            public IObject[] Objects {
                get { return new IObject[] {m_CantLoadObject}; }
            }

            public object[] PropertyObjects {
                get { return ProjectService.EmptyObjects; }
            }

            public void RemoveFolder(IFolder folder) {}

            public void RemoveObject(IObject objectd) {}

            public IFolder[] SubFolders {
                get { return new IFolder[] {}; }
            }
        }

        public bool MoveObject(IFolder folderToMove, IFolder oldFolder, IFolder folderToMoveInto) {
            return false;
        }

        public bool MoveObject(IObject objToMove, IFolder oldFolder, IFolder folderToMoveInto) {
            return false;
        }

        public void OpenObject(IObject objToOpen) {}

        public string ProjectName { get; set; }

        public object[] PropertyObjects {
            get { return ProjectService.EmptyObjects; }
        }

        public IFolder RootFolder { get; private set; }

        public void Save() {}

        public void SaveAs(string path) {}

        public DocumentMetadata OpenObjectAs(IObject objToOpen)
        {
            throw new NotImplementedException();
        }

        public CantLoadProject(string path) {
            ProjectName = path;
            RootFolder = new CantLoadFolder();
            RootFolder.Objects[0].Name = "You must have the StreamDesk.Compatability.dll file in the root of the StreamDesk directory to open .sdb or .sdx files.";
        }
    }


    public class XMLProjectGenerator : IProjectGenerator {
        public IProject Open(string projectFilePath) {
            if (!Initialize.CompatabilityLoaded && (Path.GetExtension(projectFilePath) == ".sdb" || Path.GetExtension(projectFilePath) == ".sdx")) {
                MessageBox.Show("You must have the StreamDesk.Compatability.dll file in the root of the StreamDesk directory to open .sdb or .sdx files.", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return new CantLoadProject(projectFilePath);
            }
            EditorCore.Project = new XMLFormat(projectFilePath);
            return EditorCore.Project;
        }
    }

    public class XMLFormat : IProject, ISolutionExplorerRightClick, ISolutionExplorerRename {
        private readonly StreamsDatabase _database;
        private string m_DatabasePath;

        public void Save() {
            _database.SaveDatabase(m_DatabasePath);
        }

        public bool MoveObject(IObject objToMove, IFolder oldFolder, IFolder folderToMoveInto) {
            if (!(objToMove is EditorItem)) return false;
            if (!((EditorItem) folderToMoveInto).IsProvider) return false;
            if (!((EditorItem) oldFolder).IsProvider) return false;

            oldFolder.RemoveObject(objToMove);
            folderToMoveInto.AddObject(objToMove);
            return true;
        }

        public bool MoveObject(IFolder folderToMove, IFolder oldFolder, IFolder folderToMoveInto) {
            if (!(folderToMove is EditorItem)) return false;
            if (!(folderToMoveInto is StreamsFolder) && !((EditorItem)folderToMoveInto).IsProvider) return false;
            if (!(oldFolder is StreamsFolder) && !((EditorItem) oldFolder).IsProvider) return false;

            oldFolder.RemoveFolder(folderToMove);
            folderToMoveInto.AddFolder(folderToMove);
            return true;
        }

        public void OpenObject(IObject objToOpen) {
            if (objToOpen.Object is StreamEmbed || objToOpen.Object is Stream) {
                var docMeta = new DocumentMetadata {IsFile = false, FilePath = objToOpen.Name, RequestedFormat = objToOpen.DocumentType};
                docMeta.Metadata.Add(objToOpen.Object);
                var svc = (IDevEnvSolutionSvc) DevEnvSvc.GetService(DevEnvSvc.SolutionSvc);
                svc.OpenDocument(docMeta);
            }
        }

        public string ProjectName { get { return _database.Name; } set { _database.Name = value; } }
        public IFolder RootFolder { get; private set; }
        public object[] PropertyObjects { get; private set; }

        public XMLFormat(string path) {
            _database = StreamsDatabase.OpenDatabase(path);
            m_DatabasePath = path;

            RootFolder = new SdRootFolder(_database, this);
        }

        public ContextMenuStrip RightClick(TreeNode node) {
            var retMan = new ContextMenuStrip();

            var prop = new ToolStripMenuItem("Properties");
            prop.Click += prop_Click;
            retMan.Items.Add(prop);
            return retMan;
        }

        void prop_Click(object sender, EventArgs e)
        {
            var providerDoc = new DocumentMetadata();
            providerDoc.Metadata.Add(_database);
            providerDoc.FilePath = _database.Name;
            providerDoc.RequestedFormat = new Guid("{CBE46723-F713-4DE3-B9F0-91736E7B2118}");

            var svc = (IDevEnvSolutionSvc)DevEnvSvc.GetService(DevEnvSvc.SolutionSvc);
            svc.OpenDocument(providerDoc);
        }


        public void SaveAs(string path) {
            var ext = Path.GetExtension(path);

            switch (ext) {
                case ".sdnx":
                case ".sdx":
                case ".sdb":
                    m_DatabasePath = path;
                    Save();
                    break;
            }
        }

        public bool Rename(string newName) {
            _database.Name = newName;
            return true;
        }

        public DocumentMetadata OpenObjectAs(IObject objToOpen)
        {
            throw new NotImplementedException();
        }
    }
}
