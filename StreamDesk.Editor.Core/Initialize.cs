﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Windows.Forms;
using System.Windows.Forms.VisualStyles;
using NasuTek.DevEnvironment.Extensibility;
using NasuTek.DevEnvironment.MenuCommands;
using NasuTek.DevEnvironment.Pads;
using NasuTek.M3.Streams;
using MenuItem = NasuTek.DevEnvironment.Extensibility.MenuItem;

namespace StreamDesk.Editor.Core
{
    public class EditorCore : IPackage
    {
        public static XMLFormat Project { get; set; }

        public void Load()
        {
            var plugSvc = (IDevEnvPackageSvc)DevEnvSvc.GetService(DevEnvSvc.PackageSvc);
            var uiSvc = (IDevEnvUISvc)DevEnvSvc.GetService(DevEnvSvc.UISvc);

            plugSvc.AddProduct(new Product("NasuTek StreamDesk Database Editor", "NasuTek StreamDesk Database Editor", new Icon(Properties.Resources.sd, 32, 32).ToBitmap()));
            plugSvc.AddUpdate(new Update("SD DevEnv Addin Engine Update"));

            // Load Pads
            uiSvc.RegisterPane(new SolutionExplorer());
            var solutionSvc = (IDevEnvSolutionSvc) DevEnvSvc.GetService(DevEnvSvc.SolutionSvc);
            solutionSvc.HideSolutionRoot = true;

            // Create Basic Menu
            uiSvc.AddRootMenuItem(new MenuItem("File", "File", null));
            var fileMenu = uiSvc.GetRootMenuItem("File");
            fileMenu.SubItems.Add(new MenuItem("New", "New", new NewProject()));
            fileMenu.SubItems.Add(new MenuItem("Seperator1", null, null));
            fileMenu.SubItems.Add(new MenuItem("Open", "Open", new OpenProject()));
            fileMenu.SubItems.Add(new MenuItem("Seperator2", null, null));
            fileMenu.SubItems.Add(new MenuItem("Save", "Save", new SaveProject()));
            fileMenu.SubItems.Add(new MenuItem("SaveAs", "Save As", new SaveAsProject()));
            fileMenu.SubItems.Add(new MenuItem("Seperator3", null, null));
            fileMenu.SubItems.Add(new MenuItem("Exit", "Exit", new ExitDevEnv()));

            uiSvc.AddRootMenuItem(new MenuItem("Help", "Help", null));
            var helpMenu = uiSvc.GetRootMenuItem("Help");
            helpMenu.SubItems.Add(new MenuItem("About", "About StreamDesk Database Editor", new AboutMenuItem()));

            plugSvc.AttachCommand(DevEnvSvc.CmdAfterInitialization, new Initialize());
        }
    }

    public class Initialize : AbstractCommand
    {
        public static bool CompatabilityLoaded { get; private set; }
        public override void Run()
        {
            StreamsCore.FormatterEngine.Formatters.Add(new NasuTek.M3.Streams.DatabaseFormats.XmlFormatter());
            if (File.Exists(Path.Combine(Application.StartupPath, "StreamDesk.Compatability.dll")))
            {
                Assembly compatDict = Assembly.LoadFile(Path.Combine(Application.StartupPath, "StreamDesk.Compatability.dll"));
                var sdFormatter = (IDatabaseFormatter)Activator.CreateInstance(compatDict.GetType("StreamDesk.Compatability.DatabaseFormats.SdClassicXmlFormatter"));
                var sdFormatterBinary = (IDatabaseFormatter)Activator.CreateInstance(compatDict.GetType("StreamDesk.Compatability.DatabaseFormats.SdClassicBinaryFormatter"));

                StreamsCore.FormatterEngine.Formatters.Add(sdFormatter);
                StreamsCore.FormatterEngine.Formatters.Add(sdFormatterBinary);

                CompatabilityLoaded = true;
            }
        }
    }
}
