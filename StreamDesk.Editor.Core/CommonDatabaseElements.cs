﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using NasuTek.DevEnvironment;
using NasuTek.DevEnvironment.Extensibility;
using NasuTek.DevEnvironment.Extensibility.Project;
using NasuTek.DevEnvironment.Extensibility.Workbench;
using NasuTek.DevEnvironment.Pads;
using NasuTek.M3.Streams.Database;
using NasuTek.M3.Streams.UiInterfaces;

namespace StreamDesk.Editor.Core {
    public class ChatEmbedFolder : IFolder, ISolutionExplorerRightClick {
        private StreamsDatabase m_Database;

        public ChatEmbedFolder(StreamsDatabase database) {
            m_Database = database;
        }

        public void AddFolder(IFolder folder) {}

        public void AddObject(IObject objectd) {
            m_Database.ChatEmbeds.Add((ChatEmbed) objectd.Object);
        }

        public string Name {
            get { return "Chat Embeds"; }
            set { }
        }

        public IObject[] Objects {
            get { return m_Database.ChatEmbeds.Select(i => new ChatEmbedObject(i)).Cast<IObject>().ToArray(); }
        }

        public object[] PropertyObjects {
            get { return ProjectService.EmptyObjects; }
        }

        public void RemoveFolder(IFolder folder) {}

        public void RemoveObject(IObject objectd) {
            m_Database.ChatEmbeds.Remove((ChatEmbed)objectd.Object);
        }

        public IFolder[] SubFolders {
            get { return new IFolder[] {}; }
        }

        public ContextMenuStrip RightClick(TreeNode node) {
            var cms = new ContextMenuStrip();
            var addEmbed = new ToolStripMenuItem("Add Embed");
            addEmbed.Click += addEmbed_Click;
            addEmbed.Tag = Tuple.Create(node, m_Database);
            cms.Items.Add(addEmbed);

            return cms;
        }

        private void addEmbed_Click(object sender, EventArgs e) {
            var newStream = new ChatEmbed();
            var newObjDiag = new NewObject(newStream);
            var tagObj = (Tuple<TreeNode, StreamsDatabase>) ((ToolStripMenuItem) sender).Tag;

            if (newObjDiag.ShowDialog() != DialogResult.OK) return;

            newStream.Name = newObjDiag.ObjectName;
            var chatObj = new ChatEmbedObject(newStream);

            var svc = (IDevEnvSolutionSvc)DevEnvSvc.GetService(DevEnvSvc.SolutionSvc);
            svc.AddObject(tagObj.Item1, chatObj, EditorCore.Project);
            tagObj.Item1.Expand();
        }
    }

    [DeletableObject]
    public class ChatEmbedObject : IObject, ISolutionExplorerRename, ISolutionExplorerRightClick {
        private ChatEmbed m_Embed;

        public string Name {
            get { return m_Embed.Name; }
            set { m_Embed.Name = value; }
        }

        public object Object {
            get { return m_Embed; }
            set { m_Embed = (ChatEmbed) value; }
        }

        public object[] PropertyObjects {
            get { return ProjectService.EmptyObjects; }
        }

        public Guid DocumentType {
            get { return new Guid("{0E7AA1CD-0B88-4A78-9361-7BFD1E11BA64}"); }
        }

        public ChatEmbedObject(ChatEmbed embed) {
            m_Embed = embed;
        }

        public bool Rename(string newName) {
            m_Embed.Name = newName;
            return true;
        }

        public ContextMenuStrip RightClick(TreeNode node) {
            var cms = new ContextMenuStrip();
            var removeEmbed = new ToolStripMenuItem("Remove Embed");
            removeEmbed.Click += removeEmbed_Click;
            removeEmbed.Tag = Tuple.Create(node, node.Parent);
            cms.Items.Add(removeEmbed);

            return cms;
        }

        private void removeEmbed_Click(object sender, EventArgs e)
        {
            var svc = (IDevEnvUISvc) DevEnvSvc.GetService(DevEnvSvc.UISvc);
            ((SolutionExplorer) svc.GetPane("SolutionExplorer")).DeleteSelectedNode();
        }
    }

    public class StreamsEmbedFolder : IFolder,ISolutionExplorerRightClick {
        private StreamsDatabase m_Database;

        public StreamsEmbedFolder(StreamsDatabase database) {
            m_Database = database;
        }

        public void AddFolder(IFolder folder) {}

        public void AddObject(IObject objectd) {
            m_Database.StreamEmbeds.Add((StreamEmbed)objectd.Object);
        }

        public string Name {
            get { return "Stream Embeds"; }
            set { }
        }

        public IObject[] Objects {
            get { return m_Database.StreamEmbeds.Select(i => new StreamEmbedObject(i)).Cast<IObject>().ToArray(); }
        }

        public object[] PropertyObjects {
            get { return ProjectService.EmptyObjects; }
        }

        public void RemoveFolder(IFolder folder) {}

        public void RemoveObject(IObject objectd) {
            m_Database.StreamEmbeds.Remove((StreamEmbed)objectd.Object);
        }

        public IFolder[] SubFolders {
            get { return new IFolder[] {}; }
        }

        public ContextMenuStrip RightClick(TreeNode node)
        {
            var cms = new ContextMenuStrip();
            var addEmbed = new ToolStripMenuItem("Add Embed");
            addEmbed.Click += addEmbed_Click;
            addEmbed.Tag = Tuple.Create(node, m_Database);
            cms.Items.Add(addEmbed);

            return cms;
        }

        private void addEmbed_Click(object sender, EventArgs e)
        {
            var newStream = new StreamEmbed();
            var newObjDiag = new NewObject(newStream);
            var tagObj = (Tuple<TreeNode, StreamsDatabase>)((ToolStripMenuItem)sender).Tag;

            if (newObjDiag.ShowDialog() != DialogResult.OK) return;

            newStream.Name = newObjDiag.ObjectName;
            var chatObj = new StreamEmbedObject(newStream);

            var svc = (IDevEnvSolutionSvc)DevEnvSvc.GetService(DevEnvSvc.SolutionSvc);
            svc.AddObject(tagObj.Item1, chatObj, EditorCore.Project);

            tagObj.Item1.Expand();
        }
    }

    [DeletableObject]
    public class StreamEmbedObject : IObject, ISolutionExplorerRename, ISolutionExplorerRightClick
    {
        private StreamEmbed m_Embed;

        public string Name {
            get { return m_Embed.Name; }
            set { m_Embed.Name = value; }
        }

        public object Object {
            get { return m_Embed; }
            set { m_Embed = (StreamEmbed) value; }
        }

        public object[] PropertyObjects {
            get { return ProjectService.EmptyObjects; }
        }

        public Guid DocumentType {
            get { return new Guid("{0E7AA1CD-0B88-4A78-9361-7BFD1E11BA64}"); }
        }

        public StreamEmbedObject(StreamEmbed embed) {
            m_Embed = embed;
        }

        public bool Rename(string newName) {
            m_Embed.Name = newName;
            return true;
        }

        public ContextMenuStrip RightClick(TreeNode node)
        {
            var cms = new ContextMenuStrip();
            var removeEmbed = new ToolStripMenuItem("Remove Embed");
            removeEmbed.Click += removeEmbed_Click;
            removeEmbed.Tag = Tuple.Create(node, node.Parent);
            cms.Items.Add(removeEmbed);

            return cms;
        }

        private void removeEmbed_Click(object sender, EventArgs e) {
            var svc = (IDevEnvSolutionSvc)DevEnvSvc.GetService(DevEnvSvc.SolutionSvc);
            svc.DeleteSelectedNode();
        }
    }

    public class StreamsFolder : IFolder, ISolutionExplorerRightClick {
        private StreamsDatabase m_StreamsDatabase;
        private EditorItem[] m_EditorItems;

        public StreamsFolder(StreamsDatabase db) {
            m_StreamsDatabase = db;
            m_EditorItems = db.GenerateObjectDatabaseTags<EditorItem>(null);
        }

        public void AddFolder(IFolder folder) {
            m_StreamsDatabase.Root.SubProviders.Add(((EditorItem)folder).ProviderObject);
            m_EditorItems = m_StreamsDatabase.GenerateObjectDatabaseTags<EditorItem>(null);
        }

        public void AddObject(IObject objectd) {
            m_StreamsDatabase.Root.Streams.Add(((EditorItem)objectd).StreamObject);
            m_EditorItems = m_StreamsDatabase.GenerateObjectDatabaseTags<EditorItem>(null);
        }

        public string Name {
            get { return "Streams"; }
            set { }
        }

        public IObject[] Objects {
            get { return m_EditorItems.Where(v => v.StreamObject != null).Cast<IObject>().ToArray(); }
        }

        public object[] PropertyObjects {
            get { return ProjectService.EmptyObjects; }
        }

        public void RemoveFolder(IFolder folder) {
            m_StreamsDatabase.Root.SubProviders.Remove(((EditorItem) folder).ProviderObject);
            m_EditorItems = m_StreamsDatabase.GenerateObjectDatabaseTags<EditorItem>(null);
        }

        public void RemoveObject(IObject objectd) {
            m_StreamsDatabase.Root.Streams.Remove(((EditorItem) objectd).StreamObject);
            m_EditorItems = m_StreamsDatabase.GenerateObjectDatabaseTags<EditorItem>(null);
        }

        public IFolder[] SubFolders {
            get { return m_EditorItems.Where(v => v.StreamObject == null).Cast<IFolder>().ToArray(); }
        }

        public ContextMenuStrip RightClick(TreeNode node) {
            var retVal = new ContextMenuStrip();

            var addStream = new ToolStripMenuItem("Add Stream");
            addStream.Click += addStream_Click;
            addStream.Tag = Tuple.Create(node, m_StreamsDatabase.Root, m_StreamsDatabase);
            var addProvider = new ToolStripMenuItem("Add Provider");
            addProvider.Click += addProvider_Click;
            addProvider.Tag = Tuple.Create(node, m_StreamsDatabase.Root, m_StreamsDatabase);

            retVal.Items.Add(addStream);
            retVal.Items.Add(addProvider);

            return retVal;
        }

        void addProvider_Click(object sender, EventArgs e)
        {
            var newStream = new Provider();
            var newObjDiag = new NewObject(newStream);
            var tagObj = (Tuple<TreeNode, Provider, StreamsDatabase>)((ToolStripMenuItem)sender).Tag;

            if (newObjDiag.ShowDialog() != DialogResult.OK) return;

            newStream.Name = newObjDiag.ObjectName;
            var svc = (IDevEnvSolutionSvc)DevEnvSvc.GetService(DevEnvSvc.SolutionSvc);
            svc.AddFolder(tagObj.Item1, tagObj.Item3.GenerateObjectDatabaseTag<EditorItem>(tagObj.Item2, newStream, null), EditorCore.Project);
            tagObj.Item1.Expand();
        }

        void addStream_Click(object sender, EventArgs e)
        {
            var newStream = new Stream();
            var newObjDiag = new NewObject(newStream);
            var tagObj = (Tuple<TreeNode, Provider, StreamsDatabase>)((ToolStripMenuItem)sender).Tag;

            if (newObjDiag.ShowDialog() != DialogResult.OK) return;

            newStream.Name = newObjDiag.ObjectName;
            var svc = (IDevEnvSolutionSvc)DevEnvSvc.GetService(DevEnvSvc.SolutionSvc);
            svc.AddObject(tagObj.Item1, tagObj.Item3.GenerateObjectDatabaseTag<EditorItem>(tagObj.Item2, newStream), EditorCore.Project);
            tagObj.Item1.Expand();
        }

    }

    public class SdRootFolder : IFolder {
        public SdRootFolder(StreamsDatabase db, XMLFormat proj) {
            m_StreamsFolder = new StreamsFolder(db);
            m_StreamsEmbedFolder = new StreamsEmbedFolder(db);
            m_ChatEmbedFolder = new ChatEmbedFolder(db);
        }

        private StreamsFolder m_StreamsFolder;
        private StreamsEmbedFolder m_StreamsEmbedFolder;
        private ChatEmbedFolder m_ChatEmbedFolder;

        public void AddFolder(IFolder folder) {}

        public void AddObject(IObject objectd) {}

        public string Name { get; set; }

        public IObject[] Objects {
            get { return new IObject[] {}; }
        }

        public object[] PropertyObjects {
            get { return ProjectService.EmptyObjects; }
        }

        public void RemoveFolder(IFolder folder) {}

        public void RemoveObject(IObject objectd) {}

        public IFolder[] SubFolders {
            get { return new IFolder[] {m_StreamsFolder, m_StreamsEmbedFolder, m_ChatEmbedFolder}; }
        }
    }

    [DeletableObject]
    public class EditorItem : IFolder, IObject, IObjectDatabaseTag, ISolutionExplorerRightClick, ISolutionExplorerRename {
        private MediaType _mediaType;

        public EditorItem() {
            SubItems = new List<IObjectDatabaseTag>();
        }

        #region IObjectDatabaseTag Members

        public List<IObjectDatabaseTag> SubItems { get; private set; }

        public string MenuTitle {
            get { return Name; }
            set { Name = value; }
        }

        public object[] TagObject { get; set; }

        public bool IsProvider { get; set; }

        public bool IsPinned { get; set; }

        public Stream StreamObject { get; set; }

        public Provider ProviderObject { get; set; }

        public StreamsDatabase Database { get; set; }

        public void CallSubItemsToProperArray() {}

        public Provider ParentProviderObject { get; set; }

        public MediaType MediaType { get; set; }

        #endregion

        public void AddFolder(IFolder folder) {
            if (IsProvider) {
                ProviderObject.SubProviders.Add(((EditorItem) folder).ProviderObject);
            }
        }

        public void AddObject(IObject objectd) {
            if (IsProvider) {
                ProviderObject.Streams.Add(((EditorItem) objectd).StreamObject);
            }
        }

        public string Name { get; set; }

        public IObject[] Objects {
            get { return SubItems.Where(v => !v.IsProvider).Cast<IObject>().ToArray(); }
        }

        public object[] PropertyObjects {
            get { return ProjectService.EmptyObjects; }
        }

        public void RemoveFolder(IFolder folder) {
            if (IsProvider) {
                ProviderObject.SubProviders.Remove(((EditorItem) folder).ProviderObject);
            }
        }

        public void RemoveObject(IObject objectd) {
            if (IsProvider) {
                ProviderObject.Streams.Remove(((EditorItem) objectd).StreamObject);
            }
        }

        public IFolder[] SubFolders {
            get { return SubItems.Where(v => v.IsProvider).Cast<IFolder>().ToArray(); }
        }

        public Guid DocumentType {
            get { return new Guid("{28EA2A81-8AA4-4CDE-87CE-9668ED9A7EE9}"); }
        }

        public object Object {
            get { return StreamObject; }
            set { }
        }

        public ContextMenuStrip RightClick(TreeNode node)
        {
            var retMan = new ContextMenuStrip();
            var delete = new ToolStripMenuItem("Delete");
            delete.Click += delete_Click;
            delete.Tag = node;
            retMan.Items.Add(delete);

            if (IsProvider) {
                var addStream = new ToolStripMenuItem("Add Stream");
                addStream.Click += addStream_Click;
                addStream.Tag = Tuple.Create(node, ProviderObject, Database);
                var addProvider = new ToolStripMenuItem("Add Provider");
                addProvider.Click += addProvider_Click;
                addProvider.Tag = Tuple.Create(node, ProviderObject, Database);
                var prop = new ToolStripMenuItem("Properties");
                prop.Click += prop_Click;
                retMan.Items.Add(addStream);
                retMan.Items.Add(addProvider);
                retMan.Items.Add(prop);
            }

            return retMan;
        }

        void addProvider_Click(object sender, EventArgs e)
        {
            var newStream = new Provider();
            var newObjDiag = new NewObject(newStream);
            var tagObj = (Tuple<TreeNode, Provider, StreamsDatabase>)((ToolStripMenuItem)sender).Tag;

            if (newObjDiag.ShowDialog() != DialogResult.OK) return;

            newStream.Name = newObjDiag.ObjectName;
            var svc = (IDevEnvSolutionSvc)DevEnvSvc.GetService(DevEnvSvc.SolutionSvc);
            svc.AddFolder(tagObj.Item1, tagObj.Item3.GenerateObjectDatabaseTag<EditorItem>(tagObj.Item2, newStream, null), EditorCore.Project);
            tagObj.Item1.Expand();
        }

        void addStream_Click(object sender, EventArgs e) {
            var newStream = new Stream();
            var newObjDiag = new NewObject(newStream);
            var tagObj = (Tuple<TreeNode, Provider, StreamsDatabase>) ((ToolStripMenuItem) sender).Tag;

            if (newObjDiag.ShowDialog() != DialogResult.OK) return;

            newStream.Name = newObjDiag.ObjectName;
            var svc = (IDevEnvSolutionSvc)DevEnvSvc.GetService(DevEnvSvc.SolutionSvc);
            svc.AddObject(tagObj.Item1, tagObj.Item3.GenerateObjectDatabaseTag<EditorItem>(tagObj.Item2, newStream), EditorCore.Project);
            tagObj.Item1.Expand();
        }

        void delete_Click(object sender, EventArgs e) {
            var svc = (IDevEnvSolutionSvc)DevEnvSvc.GetService(DevEnvSvc.SolutionSvc);
            svc.DeleteSelectedNode();
        }

        private void prop_Click(object sender, EventArgs e) {
            var providerDoc = new DocumentMetadata();
            providerDoc.Metadata.Add(ProviderObject);
            providerDoc.FilePath = ProviderObject.Name;
            providerDoc.RequestedFormat = new Guid("{A9A2B925-1CD7-4E22-90C7-A20103CB0DC9}");

            var svc = (IDevEnvSolutionSvc)DevEnvSvc.GetService(DevEnvSvc.SolutionSvc);
            svc.OpenDocument(providerDoc);
        }

        public bool Rename(string newName) {
            if (IsProvider)
                ProviderObject.Name = newName;
            else
                StreamObject.Name = newName;

            Name = newName;

            return true;
        }
    }
}
