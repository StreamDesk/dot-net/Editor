﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NasuTek.DevEnvironment.Extensibility;

namespace StreamDesk.Editor.Core
{
    public class AboutMenuItem:AbstractCommand
    {
        public override void Run() {
            new AboutForm().ShowDialog();
        }
    }
}
