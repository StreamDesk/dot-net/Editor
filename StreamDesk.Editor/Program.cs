﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using NasuTek.DevEnvironment;
using NasuTek.DevEnvironment.Extensibility;

namespace StreamDesk.Editor
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            DevEnv.InitializeNewDevEnv(
                new DevEnvSettings
                {
                    ProductName = "StreamDesk Database Editor",
                    ProductVersionCodebase = new Version(3, 0, 0, 0),
                    ProductVersionRelease = new Version(3, 0, 0, 0),
                    ProductBuildStage = "r1",
                    ProductBuildLab = SDEdit_Version.BuildLab,
                    ProductCopyrightYear = "2008",
                    WindowIcon = Properties.Resources.sd
                }, Environment.GetCommandLineArgs());
        }
    }
}
